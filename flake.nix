{
  description = "Development flake for the sk4rd.com site";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            direnv # Shell environment switcher
            git # Version control system
            nil # Nix language server
            nix-direnv # Direnv for Nix
            nixfmt # Nix language formatter

            # VSCodium with extensions
            (vscode-with-extensions.override {
              vscode = vscodium;
              vscodeExtensions = with vscode-extensions; [
                christian-kohler.path-intellisense
                dbaeumer.vscode-eslint
                esbenp.prettier-vscode
                ritwickdey.liveserver
              ];
            })
          ];
        };
      });
}
